![N|Solid](logo_ioasys.png)

# ioasys - Empresas

## Third-party libraries
- [Retrofit](http://square.github.io/retrofit/) by [Square](https://square.github.io/) to easily consume Restful APIs
- [Gson](https://github.com/google/gson) by Google, because of its powerful flexibility and ease of use when parsing and mapping JSON
- [Glide](https://bumptech.github.io/glide/), I didn't use it, but I chose it because of its small memory footprint compared to Picasso, bitmap pooling and ease of use

## What would I do if I had more time?
- Improve search bar layout
- Remove logo from ActionBar when moving to EnterpriseDetailFragment
- Make a request to "/enterprises/{id}" endpoint when moving to EnterpriseDetailFragment, instead of re-using data
- Add a cache layer to avoid making the same search more than once

## How to run
1. Clone the repository
2. Import the project to Android Studio or IntellijIDEA
3. Update dependencies and/or gradle version if necessary
4. Build the APK
5. Install it in your device :D
