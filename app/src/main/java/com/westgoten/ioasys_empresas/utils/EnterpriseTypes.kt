package com.westgoten.ioasys_empresas.utils

object EnterpriseTypes {
    val types = mapOf(
        "agro" to 1,
        "aviation" to 2,
        "biotech" to 3,
        "eco" to 4,
        "ecommerce" to 5,
        "education" to 6,
        "fashion" to 7,
        "fintech" to 8,
        "food" to 9,
        "games" to 10,
        "health" to 11,
        "iot" to 12,
        "logistics" to 13,
        "media" to 14,
        "mining" to 15,
        "products" to 16,
        "real estate" to 17,
        "service" to 18,
        "smart City" to 19,
        "social" to 20,
        "software" to 21,
        "technology" to 22,
        "tourism" to 23,
        "transport" to 24
    )
}