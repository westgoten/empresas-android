package com.westgoten.ioasys_empresas.model

data class EnterpriseType(
    val id: Int,
    val enterprise_type_name: String
)