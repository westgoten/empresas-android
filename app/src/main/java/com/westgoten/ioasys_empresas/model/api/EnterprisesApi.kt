package com.westgoten.ioasys_empresas.model.api

import com.westgoten.ioasys_empresas.model.EnterpriseBody
import com.westgoten.ioasys_empresas.model.EnterpriseQueryBody
import com.westgoten.ioasys_empresas.model.SignInBody
import com.westgoten.ioasys_empresas.model.SignInCredentials
import retrofit2.Call
import retrofit2.http.*

interface EnterprisesApi {
    companion object {
        const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
        const val ACCESS_HEADER_KEY = "access-token"
        const val CLIENT_HEADER_KEY = "client"
        const val UID_HEADER_KEY = "uid"
    }

    @Headers("Content-Type: application/json")
    @POST("users/auth/sign_in")
    fun signIn(@Body credentials: SignInCredentials): Call<SignInBody?>

    @GET("enterprises/{id}")
    fun getEnterprise(@Path("id") enterpriseId: Int,
                      @HeaderMap headers: Map<String, String?>): Call<EnterpriseBody?>

    @GET("enterprises")
    fun getEnterprises(@Query("enterprise_types") enterpriseType: Int?,
                       @Query("name") enterpriseName: String?,
                       @HeaderMap headers: Map<String, String?>): Call<EnterpriseQueryBody?>
}