package com.westgoten.ioasys_empresas.model

import android.os.Parcel
import android.os.Parcelable

data class SignInCredentials(
    val email: String?,
    val password: String?,
    @Transient var headers: Map<String, String?> = HashMap()
) : Parcelable {
    companion object CREATOR : Parcelable.Creator<SignInCredentials> {
        override fun createFromParcel(parcel: Parcel): SignInCredentials {
            return SignInCredentials(parcel)
        }

        override fun newArray(size: Int): Array<SignInCredentials?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
        parcel.readMap(headers, null)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(email)
        parcel.writeString(password)
        parcel.writeMap(headers)
    }

    override fun describeContents(): Int {
        return 0
    }
}