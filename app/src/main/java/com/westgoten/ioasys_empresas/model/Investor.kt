package com.westgoten.ioasys_empresas.model

import android.os.Parcel
import android.os.Parcelable

data class Investor(
    val id: Int,
    val investor_name: String?,
    val email: String?,
    val city: String?,
    val country: String?,
    val balance: Double,
    val photo: String?,
    val portfolio: Portfolio?,
    val portfolio_value: Double,
    val first_access: Boolean,
    val super_angel: Boolean
) : Parcelable {
    companion object CREATOR : Parcelable.Creator<Investor> {
        override fun createFromParcel(parcel: Parcel): Investor {
            return Investor(parcel)
        }

        override fun newArray(size: Int): Array<Investor?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readParcelable(Portfolio::class.java.classLoader),
        parcel.readDouble(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(investor_name)
        parcel.writeString(email)
        parcel.writeString(city)
        parcel.writeString(country)
        parcel.writeDouble(balance)
        parcel.writeString(photo)
        parcel.writeParcelable(portfolio, 0)
        parcel.writeDouble(portfolio_value)
        parcel.writeByte(if (first_access) 1 else 0)
        parcel.writeByte(if (super_angel) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }
}