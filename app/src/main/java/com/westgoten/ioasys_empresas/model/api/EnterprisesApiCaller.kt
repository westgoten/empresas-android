package com.westgoten.ioasys_empresas.model.api

import com.google.gson.GsonBuilder
import com.westgoten.ioasys_empresas.model.EnterpriseBody
import com.westgoten.ioasys_empresas.model.EnterpriseQueryBody
import com.westgoten.ioasys_empresas.model.SignInBody
import com.westgoten.ioasys_empresas.model.SignInCredentials
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Modifier

object EnterprisesApiCaller {
    private val enterprisesApi: EnterprisesApi

    init {
        enterprisesApi = createEnterprisesApi()
    }

    private fun createEnterprisesApi(): EnterprisesApi {
        val retrofit = Retrofit.Builder()
            .baseUrl(EnterprisesApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create()))
            .build()

        return retrofit.create(EnterprisesApi::class.java)
    }

    fun signIn(credentials: SignInCredentials, onRequestFinishedCallback: Callback<SignInBody?>) =
        enterprisesApi.signIn(credentials).enqueue(onRequestFinishedCallback)

    fun getEnterprise(enterpriseId: Int, headers: Map<String, String>,
                      onRequestFinishedCallback: Callback<EnterpriseBody?>) =
        enterprisesApi.getEnterprise(enterpriseId, headers).enqueue(onRequestFinishedCallback)

    fun getEnterprises(enterpriseType: Int?, enterpriseName: String?, headers: Map<String, String?>,
                       onRequestFinishedCallback: Callback<EnterpriseQueryBody?>): Call<EnterpriseQueryBody?> {
        val call = enterprisesApi.getEnterprises(enterpriseType, enterpriseName, headers)
        call.enqueue(onRequestFinishedCallback)
        return call
    }
}