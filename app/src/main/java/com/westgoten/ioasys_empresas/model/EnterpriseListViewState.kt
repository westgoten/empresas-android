package com.westgoten.ioasys_empresas.model

sealed class EnterpriseListViewState {
    object Initial : EnterpriseListViewState()
    object SearchMenu : EnterpriseListViewState()
    object Loading : EnterpriseListViewState()
    data class Data(val enterpriseSummaryList: List<EnterpriseSummary>?) : EnterpriseListViewState()
    data class Error(val errorMessage: String? = null) : EnterpriseListViewState()
    data class MovingToEnterpriseDetail(val previousState: EnterpriseListViewState?) : EnterpriseListViewState()
}