package com.westgoten.ioasys_empresas.model

data class EnterpriseQueryBody(
    val enterprises: List<EnterpriseSummary>
)