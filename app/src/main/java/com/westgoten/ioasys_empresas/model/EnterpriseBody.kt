package com.westgoten.ioasys_empresas.model

data class EnterpriseBody(
    val enterprise: EnterpriseDetails
)