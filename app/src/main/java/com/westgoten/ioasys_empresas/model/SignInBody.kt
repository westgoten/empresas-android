package com.westgoten.ioasys_empresas.model

import android.os.Parcel
import android.os.Parcelable

data class SignInBody(
    val investor: Investor?,
    val enterprise: String?
) : Parcelable {
    companion object CREATOR : Parcelable.Creator<SignInBody> {
        override fun createFromParcel(parcel: Parcel): SignInBody {
            return SignInBody(parcel)
        }

        override fun newArray(size: Int): Array<SignInBody?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Investor::class.java.classLoader),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(investor, 0)
        parcel.writeString(enterprise)
    }

    override fun describeContents(): Int = 0
}