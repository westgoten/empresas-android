package com.westgoten.ioasys_empresas.model

import android.os.Parcel
import android.os.Parcelable

data class Portfolio(
    val enterprises_number: Int,
    val enterprises: List<String>?
) : Parcelable {
    companion object CREATOR : Parcelable.Creator<Portfolio> {
        override fun createFromParcel(parcel: Parcel): Portfolio {
            return Portfolio(parcel)
        }

        override fun newArray(size: Int): Array<Portfolio?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.createStringArrayList()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(enterprises_number)
        parcel.writeStringList(enterprises)
    }

    override fun describeContents(): Int {
        return 0
    }
}