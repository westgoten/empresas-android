package com.westgoten.ioasys_empresas.model

data class RequestResponse(
    val isSuccessful: Boolean,
    val errorMessage: String? = null
)