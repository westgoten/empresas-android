package com.westgoten.ioasys_empresas.model

data class ToolbarState(
    val isLogoVisible: Boolean,
    val title: String? = null
)