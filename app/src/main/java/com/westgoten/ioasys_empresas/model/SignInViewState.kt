package com.westgoten.ioasys_empresas.model

import com.westgoten.ioasys_empresas.Event

data class SignInViewState(
    val buttonEnabled: Boolean = true,
    val requestResponse: Event<RequestResponse>? = null
)