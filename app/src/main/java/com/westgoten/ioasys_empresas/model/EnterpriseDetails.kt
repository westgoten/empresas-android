package com.westgoten.ioasys_empresas.model

data class EnterpriseDetails(
    val id: Int,
    val enterprise_name: String,
    val description: String,
    val email_enterprise: String?,
    val facebook: String?,
    val twitter: String?,
    val linkedin: String?,
    val phone: String?,
    val own_enterprise: Boolean,
    val photo: String?,
    val value: Int,
    val shares: Int,
    val share_price: Double,
    val own_shares: Int,
    val city: String,
    val country: String,
    val enterprise_type: EnterpriseType
)