package com.westgoten.ioasys_empresas.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.westgoten.ioasys_empresas.R
import com.westgoten.ioasys_empresas.model.ToolbarState
import com.westgoten.ioasys_empresas.viewmodel.EnterprisesHomeViewModel

class EnterpriseDetailFragment : Fragment() {
    private lateinit var viewModel: EnterprisesHomeViewModel
    private lateinit var enterpriseBrand: TextView
    private lateinit var enterpriseDescription: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity as ViewModelStoreOwner, ViewModelProvider.NewInstanceFactory())
            .get(EnterprisesHomeViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_enterprise_detail, container, false)
        enterpriseBrand = rootView.findViewById(R.id.enterprise_detail_brand)
        enterpriseDescription = rootView.findViewById(R.id.enterprise_detail_description)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.observableEnterpriseDetail.observe(this, Observer {
            if (it != null) {
                enterpriseBrand.text = viewModel.getEnterpriseBrand(it.enterprise_name)
                enterpriseDescription.text = it.description
                viewModel.setToolbarState(ToolbarState(false, it.enterprise_name))
            }
        })
    }
}