package com.westgoten.ioasys_empresas.view.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.westgoten.ioasys_empresas.R
import com.westgoten.ioasys_empresas.utils.hideSoftKeyboard
import com.westgoten.ioasys_empresas.viewmodel.SignInViewModel

class SignInActivity : AppCompatActivity() {
    companion object {
        const val SIGN_IN_BODY = "SignInActivity.SIGN_IN_BODY"
        const val SIGN_IN_CREDENTIALS = "SignInActivity.SIGN_IN_CREDENTIALS"
    }

    private lateinit var viewModel: SignInViewModel
    private lateinit var email: EditText
    private lateinit var password: EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())
            .get(SignInViewModel::class.java)
        email = findViewById(R.id.sign_in_email)
        password = findViewById(R.id.sign_in_password)
        setUpSignInButton()
        setSignInViewStateObserver()
    }

    private fun setUpSignInButton() {
        button = findViewById(R.id.sign_in_button)
        button.setOnClickListener {
            if (isInputValid()) {
                viewModel.setSignInButtonEnabled(false)
                it.hideSoftKeyboard()
                viewModel.signIn(email.text.toString(), password.text.toString())
            }
        }
    }

    private fun isInputValid(): Boolean {
        var isValid = true
        if (!viewModel.isInputValid(email.text.toString())) {
            email.error = getString(R.string.sign_in_error_empty_input)
            isValid = false
        }
        if (!viewModel.isInputValid(password.text.toString())) {
            password.error = getString(R.string.sign_in_error_empty_input)
            isValid = false
        }
        return isValid
    }

    private fun setSignInViewStateObserver() {
        viewModel.observableSignInViewState.observe(this, Observer { signInViewState ->
            if (signInViewState != null) {
                button.isEnabled = signInViewState.buttonEnabled
                signInViewState.requestResponse?.getContentIfNotHandled()?.let {
                    if (it.isSuccessful)
                        moveToEnterprisesHomeActivity()
                    else
                        Toast.makeText(
                            this, it.errorMessage ?: getString(R.string.error_connection_failed),
                            Toast.LENGTH_LONG
                        ).show()
                }
            }
        })
    }

    private fun moveToEnterprisesHomeActivity() {
        startActivity(
            Intent().putExtra(SIGN_IN_BODY, viewModel.signInBody)
                .putExtra(SIGN_IN_CREDENTIALS, viewModel.signInCredentials)
                .setClass(this, EnterprisesHomeActivity::class.java)
        )
        finish()
    }
}
