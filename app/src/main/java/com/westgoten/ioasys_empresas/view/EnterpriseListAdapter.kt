package com.westgoten.ioasys_empresas.view

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.westgoten.ioasys_empresas.model.EnterpriseSummary
import com.westgoten.ioasys_empresas.R
import com.westgoten.ioasys_empresas.viewmodel.EnterprisesHomeViewModel

class EnterpriseListAdapter(
    private val viewModel: EnterprisesHomeViewModel
) : RecyclerView.Adapter<EnterpriseListAdapter.ViewHolder>() {

    private var dataSet: List<EnterpriseSummary>? = null

    class ViewHolder(
        val item: CardView,
        val enterpriseBrand: TextView,
        val enterpriseName: TextView,
        val enterpriseType: TextView,
        val enterpriseCountry: TextView
    ) : RecyclerView.ViewHolder(item)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_enterprises_recycler_view, parent, false) as CardView
        return ViewHolder(itemView,
            itemView.findViewById(R.id.enterprise_brand),
            itemView.findViewById(R.id.enterprise_name),
            itemView.findViewById(R.id.enterprise_type),
            itemView.findViewById(R.id.enterprise_country))
    }

    override fun getItemCount() = dataSet?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val enterpriseSummary = dataSet?.get(position)
        val enterpriseNameText = enterpriseSummary?.enterprise_name
        holder.enterpriseName.text = enterpriseNameText
        holder.enterpriseCountry.text = enterpriseSummary?.country
        holder.enterpriseType.text = enterpriseSummary?.enterprise_type?.enterprise_type_name
        holder.enterpriseBrand.text = viewModel.getEnterpriseBrand(enterpriseNameText)
        holder.item.setOnClickListener {
            viewModel.moveToEnterpriseDetail(enterpriseSummary)
        }
    }

    fun updateDataSet(newDataSet: List<EnterpriseSummary>?) {
        dataSet = newDataSet
        notifyDataSetChanged()
    }
}