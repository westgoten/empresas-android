package com.westgoten.ioasys_empresas.view.fragments

import android.os.Bundle
import android.view.*
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.westgoten.ioasys_empresas.R
import com.westgoten.ioasys_empresas.model.EnterpriseListViewState
import com.westgoten.ioasys_empresas.model.ToolbarState
import com.westgoten.ioasys_empresas.utils.hideSoftKeyboard
import com.westgoten.ioasys_empresas.view.EnterpriseListAdapter
import com.westgoten.ioasys_empresas.viewmodel.EnterprisesHomeViewModel

class EnterpriseListFragment : Fragment() {
    private lateinit var loading: ProgressBar
    private lateinit var searchInitialMessage: TextView
    private lateinit var viewModel: EnterprisesHomeViewModel
    private lateinit var adapter: EnterpriseListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProvider(activity as ViewModelStoreOwner, ViewModelProvider.NewInstanceFactory())
            .get(EnterprisesHomeViewModel::class.java)
        adapter = EnterpriseListAdapter(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_enterprise_list, container, false)
        loading = rootView.findViewById(R.id.enterprise_list_loading)
        searchInitialMessage = rootView.findViewById(R.id.enterprise_list_search_message)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setEnterpriseListViewStateObserver(view as ViewGroup)
        viewModel.setToolbarState(ToolbarState(isLogoVisible = true))
    }

    private fun setEnterpriseListViewStateObserver(rootView: ViewGroup) {
        viewModel.observableEnterpriseListViewState.observe(this, Observer {
            when (it) {
                is EnterpriseListViewState.Initial -> setUpInitialState(rootView)
                is EnterpriseListViewState.SearchMenu -> setUpSearchMenuState(rootView)
                is EnterpriseListViewState.Loading -> setUpLoadingState(rootView)
                is EnterpriseListViewState.Error -> setUpErrorState(it)
                is EnterpriseListViewState.Data -> setUpDataState(it, rootView)
                is EnterpriseListViewState.MovingToEnterpriseDetail -> moveToEnterpriseDetail(it)
            }
        })
    }

    private fun setUpInitialState(rootView: ViewGroup) {
        viewModel.cancelEnterpriseListFetching()
        searchInitialMessage.visibility = View.VISIBLE
        loading.visibility = View.GONE
        removeRecyclerView(rootView)
    }

    private fun setUpSearchMenuState(rootView: ViewGroup) {
        searchInitialMessage.visibility = View.GONE
        removeRecyclerView(rootView)
    }

    private fun setUpLoadingState(rootView: ViewGroup) {
        loading.visibility = View.VISIBLE
        searchInitialMessage.visibility = View.GONE
        removeRecyclerView(rootView)
    }

    private fun setUpErrorState(errorState: EnterpriseListViewState.Error) {
        Toast.makeText(context, errorState.errorMessage ?: getString(R.string.error_connection_failed),
            Toast.LENGTH_LONG).show()
        loading.visibility = View.GONE
        viewModel.setEnterpriseListViewState(EnterpriseListViewState.SearchMenu)
    }

    private fun removeRecyclerView(rootView: ViewGroup) {
        if (rootView.childCount >= 3)
            rootView.removeViewAt(2)
    }

    private fun setUpDataState(dataState: EnterpriseListViewState.Data, rootView: ViewGroup) {
        loading.visibility = View.GONE
        searchInitialMessage.visibility = View.GONE
        adapter.updateDataSet(dataState.enterpriseSummaryList)
        if (rootView.childCount < 3)
            setUpRecyclerView(rootView)
    }

    private fun setUpRecyclerView(rootView: ViewGroup) {
        context?.let {
            AsyncLayoutInflater(it).inflate(R.layout.enterprises_recycler_view, rootView) { view, resId, container ->
                container?.addView(view)
                val enterpriseList = view as RecyclerView
                enterpriseList.layoutManager = LinearLayoutManager(context)
                enterpriseList.setHasFixedSize(true)
                enterpriseList.adapter = adapter
            }
        }
    }

    private fun moveToEnterpriseDetail(movingState: EnterpriseListViewState.MovingToEnterpriseDetail) {
        viewModel.setEnterpriseListViewState(movingState.previousState)
        openEnterpriseDetailFragment()
    }

    private fun openEnterpriseDetailFragment() {
        fragmentManager?.beginTransaction()
            ?.replace(R.id.fragment_container, EnterpriseDetailFragment())
            ?.addToBackStack(null)
            ?.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val menuItem = menu.findItem(R.id.search)
        menuItem.setOnActionExpandListener(createOnActionExpandListener())
        setUpSearchView(menuItem)
    }

    private fun createOnActionExpandListener(): MenuItem.OnActionExpandListener {
        return object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                if (viewModel.getEnterpriseListViewState() is EnterpriseListViewState.Initial)
                    viewModel.setEnterpriseListViewState(EnterpriseListViewState.SearchMenu)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                viewModel.setEnterpriseListViewState(EnterpriseListViewState.Initial)
                return true
            }
        }
    }

    private fun setUpSearchView(menuItem: MenuItem) {
        val searchView = menuItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.hideSoftKeyboard()
                viewModel.setEnterpriseListViewState(EnterpriseListViewState.Loading)
                viewModel.performSearch(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val menuItem = menu.findItem(R.id.search)
        when (viewModel.getEnterpriseListViewState()) {
            is EnterpriseListViewState.Initial -> menuItem.collapseActionView()
            else -> menuItem.expandActionView()
        }
    }
}