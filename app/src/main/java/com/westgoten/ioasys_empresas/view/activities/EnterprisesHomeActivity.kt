package com.westgoten.ioasys_empresas.view.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.westgoten.ioasys_empresas.R
import com.westgoten.ioasys_empresas.model.ToolbarState
import com.westgoten.ioasys_empresas.view.fragments.EnterpriseListFragment
import com.westgoten.ioasys_empresas.viewmodel.EnterprisesHomeViewModel

class EnterprisesHomeActivity : AppCompatActivity() {
    private lateinit var viewModel: EnterprisesHomeViewModel
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enterprises_home)
        setUpToolbar()
        setUpViewModel()
        setToolbarStateObserver()
        addOnBackStackChangedListener()

        if (savedInstanceState == null) {
            getIntentData()
            moveToEnterpriseListFragment()
        } else
            decideUpButtonVisibility()
    }

    private fun setUpToolbar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())
            .get(EnterprisesHomeViewModel::class.java)
    }

    private fun setToolbarStateObserver() {
        viewModel.observableToolbarState.observe(this, Observer {
            if (it != null) {
                supportActionBar?.title = it.title?.toUpperCase()
                setLogoVisibility(it)
            }
        })
    }

    private fun setLogoVisibility(toolbarState: ToolbarState) {
        toolbar.getChildAt(0).isVisible = toolbarState.isLogoVisible
    }

    private fun addOnBackStackChangedListener() {
        supportFragmentManager.addOnBackStackChangedListener {
            decideUpButtonVisibility()
        }
    }

    private fun decideUpButtonVisibility() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        else
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun getIntentData() {
        viewModel.signInBody = intent.getParcelableExtra(SignInActivity.SIGN_IN_BODY)
        viewModel.signInCredentials = intent.getParcelableExtra(SignInActivity.SIGN_IN_CREDENTIALS)
    }

    private fun moveToEnterpriseListFragment() {
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container, EnterpriseListFragment())
            .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                supportFragmentManager.popBackStack()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}