package com.westgoten.ioasys_empresas.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.westgoten.ioasys_empresas.model.*
import com.westgoten.ioasys_empresas.model.api.EnterprisesApiCaller
import com.westgoten.ioasys_empresas.utils.EnterpriseTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EnterprisesHomeViewModel : ViewModel() {
    var signInBody: SignInBody? = null
    var signInCredentials: SignInCredentials? = null
    private val _observableEnterpriseListViewState = MutableLiveData<EnterpriseListViewState?>()
    val observableEnterpriseListViewState: LiveData<EnterpriseListViewState?>
        get() = _observableEnterpriseListViewState
    private var fetchEnterprisesCall: Call<EnterpriseQueryBody?>? = null

    private val _observableEnterpriseDetail = MutableLiveData<EnterpriseSummary?>()
    val observableEnterpriseDetail: LiveData<EnterpriseSummary?>
        get() = _observableEnterpriseDetail

    private val _observableToolbarState = MutableLiveData<ToolbarState>()
    val observableToolbarState: LiveData<ToolbarState>
        get() = _observableToolbarState

    init {
        setEnterpriseListViewState(EnterpriseListViewState.Initial)
        setToolbarState(ToolbarState(isLogoVisible = true))
    }

    fun performSearch(query: String?) {
        val enterpriseType = EnterpriseTypes.types[query?.toLowerCase()]
        val enterpriseName = if (enterpriseType == null) query else null // Search for enterprise type or enterprise name, not both
        fetchEnterpriseListFromServer(enterpriseType, enterpriseName)
    }

    private fun fetchEnterpriseListFromServer(enterpriseType: Int?, enterpriseName: String?) {
        fetchEnterprisesCall = EnterprisesApiCaller.getEnterprises(enterpriseType, enterpriseName, getHeaders(),
            object : Callback<EnterpriseQueryBody?> {
                override fun onFailure(call: Call<EnterpriseQueryBody?>, t: Throwable) {
                    if (!call.isCanceled)
                        setEnterpriseListViewState(EnterpriseListViewState.Error())
                }

                override fun onResponse(call: Call<EnterpriseQueryBody?>, response: Response<EnterpriseQueryBody?>) {
                    if (response.isSuccessful) {
                        val enterpriseSummaryList = response.body()?.enterprises
                        setEnterpriseListViewState(EnterpriseListViewState.Data(enterpriseSummaryList))
                    } else {
                        setEnterpriseListViewState(EnterpriseListViewState.Error(
                            "Error ${response.code()}: " + response.message()))
                    }
                }

            })
    }

    private fun getHeaders(): Map<String, String?> = signInCredentials?.headers ?: emptyMap()

    fun setEnterpriseListViewState(viewState: EnterpriseListViewState?) {
        _observableEnterpriseListViewState.value = viewState
    }

    fun getEnterpriseListViewState(): EnterpriseListViewState? = observableEnterpriseListViewState.value

    fun cancelEnterpriseListFetching() {
        fetchEnterprisesCall?.cancel()
    }

    fun moveToEnterpriseDetail(enterpriseDetailData: EnterpriseSummary?) {
        setEnterpriseListViewState(EnterpriseListViewState.MovingToEnterpriseDetail(getEnterpriseListViewState()))
        _observableEnterpriseDetail.value = enterpriseDetailData
    }

    fun setToolbarState(newState: ToolbarState) {
        _observableToolbarState.value = newState
    }

    fun getEnterpriseBrand(enterpriseNameText: String?): String? {
        var result: String? = null
        if (!enterpriseNameText.isNullOrEmpty()) {
            result = addFirstLetter(enterpriseNameText)
            result = addSecondLetter(enterpriseNameText, result)
        }
        return result
    }

    private fun addFirstLetter(enterpriseNameText: String) = enterpriseNameText.substring(0, 1)

    private fun addSecondLetter(enterpriseNameText: String, result: String): String {
        var result1 = result
        val charArray = enterpriseNameText.substring(1).toCharArray()
        for (char in charArray) {
            if (char.isUpperCase()) {
                result1 += char
                break
            }
        }
        if (result1.length < 2)
            result1 += enterpriseNameText.last()
        return result1
    }
}