package com.westgoten.ioasys_empresas.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.westgoten.ioasys_empresas.Event
import com.westgoten.ioasys_empresas.model.RequestResponse
import com.westgoten.ioasys_empresas.model.SignInBody
import com.westgoten.ioasys_empresas.model.SignInCredentials
import com.westgoten.ioasys_empresas.model.SignInViewState
import com.westgoten.ioasys_empresas.model.api.EnterprisesApi
import com.westgoten.ioasys_empresas.model.api.EnterprisesApiCaller
import okhttp3.Headers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignInViewModel : ViewModel() {
    private val _observableSignInViewState = MutableLiveData<SignInViewState?>()
    val observableSignInViewState: LiveData<SignInViewState?>
        get() = _observableSignInViewState
    var signInBody: SignInBody? = null
    var signInCredentials: SignInCredentials? = null

    fun signIn(email: String, password: String) {
        EnterprisesApiCaller.signIn(SignInCredentials(email, password), object : Callback<SignInBody?> {
            override fun onFailure(call: Call<SignInBody?>, t: Throwable) {
                _observableSignInViewState.value =
                    SignInViewState(true, Event(RequestResponse(isSuccessful = false)))
            }

            override fun onResponse(call: Call<SignInBody?>, response: Response<SignInBody?>) {
                onResponse(response, email, password)
            }
        })
    }

    private fun onResponse(response: Response<SignInBody?>, email: String, password: String) {
        if (response.isSuccessful) {
            signInBody = response.body()
            signInCredentials = setUpSignInCredentials(email, password, response.headers())
            _observableSignInViewState.value =
                SignInViewState(false, Event(RequestResponse(isSuccessful = true)))
        } else {
            _observableSignInViewState.value = SignInViewState(true, Event(
                    RequestResponse(
                        false,
                        "Error ${response.code()}: " + response.message()
                    )
            ))
        }
    }

    private fun setUpSignInCredentials(email: String, password: String, headers: Headers): SignInCredentials {
        return SignInCredentials(
            email, password, mapOf(
                EnterprisesApi.ACCESS_HEADER_KEY to headers[EnterprisesApi.ACCESS_HEADER_KEY],
                EnterprisesApi.CLIENT_HEADER_KEY to headers[EnterprisesApi.CLIENT_HEADER_KEY],
                EnterprisesApi.UID_HEADER_KEY to headers[EnterprisesApi.UID_HEADER_KEY]
            )
        )
    }

    fun isInputValid(input: String?): Boolean = !input.isNullOrEmpty()

    fun setSignInButtonEnabled(enabled: Boolean) {
        _observableSignInViewState.value = _observableSignInViewState.value?.copy(buttonEnabled = enabled) ?:
                SignInViewState(buttonEnabled = enabled)
    }
}